#include "../include/DatosMemCompartida.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

int main() {
	
	char *org;
	DatosMemCompartida *datos;
	int fd_memoria;
	char *p;
	struct stat bstat;

	//ERROR AL ABRIR EL FICHERO "bot.txt"/////////////////////////
	if((fd_memoria=open("/tmp/bot.txt",O_RDWR))<0)
		perror("Error al abrir fichero");	
	if (fstat(fd_memoria, &bstat)<0) {
	perror("Error en fstat del fichero"); 
	close(fd_memoria);
   		return -1;
	}
	//Establecemos la proyección del fichero	
	org=(char*)mmap(NULL,sizeof(*(datos)), PROT_READ|PROT_WRITE ,MAP_SHARED,fd_memoria,0);
   		
	
	close(fd_memoria);//Cerramos el fichero

	datos=(DatosMemCompartida*)org;	
	while(1)
	{
		if ((datos->esfera.centro.y)>=datos->raqueta1.y1)
		{
			datos->accion=1;
		}
		else if((datos->esfera.centro.y)<=datos->raqueta1.y2)
                {
			datos->accion=-1;
		}
		
		else datos->accion=0;
		usleep(25000);
	}
	munmap(org,sizeof(*(datos)));		
}

//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "stdio.h"
#include "stdlib.h"
#include <unistd.h>
#include <fcntl.h>
#include "Socket.h"
#define TAM_BUFF 200
#define MARGEN 0.5f
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

char* org;

 CMundo::CMundo()
{
	Init();
	
}

CMundo::~CMundo()
{
	
	munmap(org,sizeof(Datos));
	servidor.Close();
	
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
	
	
	/**/
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
        for(int j=0;j<esferas.size();j++){
		esferas[j]->Dibuja();
        }
        for(int k=0;k<disparos.size();k++){
        disparos[k]->Dibuja();
        }
    fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();


	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)


{	
	
	
	char Dat[200];
	servidor.Receive(Dat,sizeof(Dat));
	sscanf(Dat,"%f %f %f %f %f %f %f %f %f %f %d %d", &esferas[0]->centro.x,&esferas[0]->centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);

	
	Datos.esfera=*esferas[0];
	Datos.raqueta1=jugador2;
	PunteroDatos->esfera=*esferas[0];
	PunteroDatos->raqueta1=jugador2;	
	if(PunteroDatos->accion==1) OnKeyboardDown('o',1,1);
	if(PunteroDatos->accion==-1) OnKeyboardDown('l',1,1);
	

}
void CMundo::OnKeyboardDown(unsigned char key,int x,int y)
{
	
	char cadena[200];
	sprintf(cadena,"%c",key);
	servidor.Send(cadena,sizeof(cadena));
	
	
	if(key == 's' && jugador1.parado==false){jugador1.velocidad.y=-4.0f;}
	if(key == 'w' && jugador1.parado==false){jugador1.velocidad.y=4.0f;}
	if(key == 'l' && jugador2.parado==false){jugador2.velocidad.y=-4.0f;}
	if(key == 'o' && jugador2.parado==false){jugador2.velocidad.y=4.0f;}
	if(key == 'f' && jugador1.parado==false){this->Disparando(jugador1);}
	if(key == 'j' && jugador2.parado==false){this->Disparando(jugador2);}
	
	
	
}

void CMundo::Init()
{
	
	//Conexión con el Servidor
	
	printf("Introduzca el nombre del Cliente\n");
	char nombre[200];
	std::cin>>nombre;
	servidor.Connect((char*)"127.0.0.1",4000);
	servidor.Send(nombre,sizeof(nombre));
	
	
	
	temporizador=0;
	esferas.push_back(new Esfera);
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6.0f;jugador1.y1=-1.0f;
	jugador1.x2=-6.0f;jugador1.y2=1.0f;
	jugador1.parado=false;
	jugador1.tiempoParado=0;
	//a la dcha
	jugador2.g=0;
	jugador2.x1=6.0f;jugador2.y1=-1.0f;
	jugador2.x2=6.0f;jugador2.y2=1.0f;
	jugador2.parado=false;
	jugador2.tiempoParado=0;
	
	// Proyección en Memoria
	
	Datos.esfera=*esferas[0];
	Datos.raqueta1=jugador2;
	
	fd_memoria=open("/tmp/bot.txt", O_CREAT|O_RDWR,0666);
	
	write(fd_memoria,&Datos,sizeof(Datos));	
	if (fstat(fd_memoria, &bstat)<0) {
	perror("Error en fstat del fichero"); 
	close(fd_memoria);
	}
	
	org=(char*)mmap(NULL,sizeof(Datos),PROT_READ|PROT_WRITE,MAP_SHARED,fd_memoria,0);
		 
		close(fd_memoria);
		PunteroDatos=(DatosMemCompartida*)org;
		PunteroDatos->accion=0;
		 
}
///////////////////////////////////////////////////////////////////////////////
void CMundo::Disparando(Raqueta &player){
	
	
	if(player.x2<0.0f){
		float posx,posy,velx,vely;

		posx=player.x2+0.5f;
		posy=(player.y2+player.y1)/2;
		velx=10.0f;
		vely=0.0f;
		disparos.push_back(new Disparo(posx,posy,velx,vely,0.15f));
		
	}
	else if(player.x2>0.0f){
		float posx,posy,velx,vely;
		
		posx=player.x2-0.5f;
		posy=(player.y2+player.y1)/2;
		velx=-10.0f;
		vely=0.0f;
		disparos.push_back(new Disparo(posx,posy,velx,vely,0.15f));
			
	}
}

bool CMundo::ImpactoP1(Raqueta &player,Disparo *d){
	
	 if(d->velocidad.x<0.0f && (d->posicion.x<player.x2+MARGEN) && (d->posicion.x>player.x2-MARGEN) && (d->posicion.y>player.y1)&&(d->posicion.y<player.y2)){
		
		return true;
	}
	else return false;
}
bool CMundo::ImpactoP2(Raqueta &player,Disparo *d){
	
	
	 if(d->velocidad.x>0.0f && (d->posicion.x>player.x2-MARGEN) && (d->posicion.x<player.x2+MARGEN) && (d->posicion.y>player.y1) && (d->posicion.y<player.y2)){
		return true;
	}
	else return false;
}
void CMundo::Paralizar(Raqueta &player){
	
	if(player.parado==true){
		player.tiempoParado++;
			if(player.tiempoParado==75){
				player.tiempoParado=0;
				player.parado=false;
				
			}
	}
}
		

//////////////////////////////////////////////////////////////////////////////////////////

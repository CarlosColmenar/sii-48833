//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "stdio.h"
#include "stdlib.h"
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <iostream>
#define TAM_BUFF 1024
#define MARGEN 0.5f
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void* hilo_comandos1(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador1();
}
void* hilo_comandos2(void* d)
{
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador2();
}
void* hilo_conexiones(void* d)
{
      CMundo* p=(CMundo*) d;
      p->GestionaConexiones();
}
 CMundo::CMundo()
{
	Init();
	
}

CMundo::~CMundo()
{
	
	write(tuberiaLogger,"F",sizeof("F"));
	close(tuberiaLogger);
	unlink("/tmp/TuberiaLogger");
	
	acabar=1;
	for(int i=0;i<conexiones.size();i++){
		conexiones[i].Close();
	}
	pthread_join(thid1,NULL);
	pthread_join(thid2,NULL);
	pthread_join(thid_conexiones,NULL);
	
		
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	void GestionaConexiones();
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
	
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
        for(int j=0;j<esferas.size();j++){
		esferas[j]->Dibuja();
        }
        for(int k=0;k<disparos.size();k++){
        disparos[k]->Dibuja();
        }
    fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();


	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)

{	
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	for(int i=0;i<esferas.size();i++){
	esferas[i]->Mueve(0.025f);}
	for(int k=0;k<disparos.size();k++){
	disparos[k]->Mueve(0.025f);}
	int i;
	for(i=0;i<paredes.size();i++){
	for (int j=0;j<esferas.size();j++){
	
		paredes[i].Rebota(*esferas[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	  }
        }
        for (int j=0;j<esferas.size();j++){
	
	jugador1.Rebota(*esferas[j]);
	jugador2.Rebota(*esferas[j]);
        }
  //////////////////////////////////////////////////////////////////////////////////   
     for(int k=0;k<disparos.size();k++){
		 
			if(ImpactoP2(jugador2,disparos[k])){
				delete disparos[k];
				disparos.erase(disparos.begin()+k);
				jugador2.velocidad.y=0.0f;
				jugador2.parado=true;
				
			}
		
		else if(ImpactoP1(jugador1,disparos[k])){
				delete disparos[k];
				disparos.erase(disparos.begin()+k);
				jugador1.velocidad.y=0.0f;
				jugador1.parado=true;
			}
			
			if(disparos[k]->posicion.x>15.0f || disparos[k]->posicion.x<-15.0f){
				delete disparos[k];
				disparos.erase(disparos.begin()+k);
			}
		}		
				
     this->Paralizar(jugador1);
     this->Paralizar(jugador2);   
 ////////////////////////////////////////////////////////////////////////////////////////       
    for (int i=0;i<esferas.size();i++){	
	if(fondo_izq.Rebota(*esferas[i]))
	{
		esferas[i]->centro.x=0;
		esferas[i]->centro.y=rand()/(float)RAND_MAX;
		esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
		esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
		++puntos2;
		char cad[200];
		sprintf(cad,"Jugador 2 marca 1 punto, lleva %d puntos", puntos2);
		write (tuberiaLogger, cad, sizeof(cad));
/////////////////////CONDICION DE SALIDA////////////////////////////////////////////////////		
		if (puntos2==5){
		printf("El ganador es el Jugador 2 tras anotar 5 puntos\n");
		exit(1);
	}
		
	}

	if(fondo_dcho.Rebota(*esferas[i]))
	{
		esferas[i]->centro.x=0;
		esferas[i]->centro.y=rand()/(float)RAND_MAX;
		esferas[i]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esferas[i]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
		++puntos1;
		char cad[200];
		sprintf(cad,"Jugador 1 marca 1 punto, lleva %d puntos", puntos1);
		write (tuberiaLogger, cad, sizeof(cad));

/////////////////////CONDICION DE SALIDA////////////////////////////////////////////////////		
		if (puntos1==5){
		printf("El ganador es el Jugador 1 tras anotar 5 puntos\n");
		exit(1);
	}
	}

	}
		
	sprintf(cadena_n,"%f %f %f %f %f %f %f %f %f %f %d %d", esferas[0]->centro.x,esferas[0]->centro.y, jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2, puntos1, puntos2);

for (int i=conexiones.size()-1;i>=0;i--) 
 {

       if(conexiones[i].Send(cadena_n,sizeof(cadena_n))<=0){
	    printf("Cliente Conectado\n");
           conexiones.erase(conexiones.begin()+i);
            if (i<2) // Hay menos de dos clientes conectados
	    {
		// Se resetean los puntos a cero
		printf("Reset de la Puntuacion\n");
		puntos2=0;
		puntos1=0;
	    }
     }
 }

}
void CMundo::OnKeyboardDown(unsigned char key,int x,int y)
{
		

}

void CMundo::Init()
{
	//Apertura de la tubería en modo escritura
	tuberiaLogger=open("/tmp/TuberiaLogger",O_WRONLY);
	
	//Inicialización del Servidor
	servidor.InitServer((char*)"127.0.0.1",4000);	
		
	//Hilos para las teclas del jugador1 y jugador2 y para la gestión de las conexiones
		
	 pthread_create(&thid1, NULL, hilo_comandos1, this);
     pthread_create(&thid2, NULL, hilo_comandos2, this);
	 pthread_create(&thid_conexiones, NULL, hilo_conexiones, this);
	
	///////////////////////////////////////////////
	acabar=0;
	temporizador=0;
	esferas.push_back(new Esfera);
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6.0f;jugador1.y1=-1.0f;
	jugador1.x2=-6.0f;jugador1.y2=1.0f;
	jugador1.parado=false;
	jugador1.tiempoParado=0;
	//a la dcha
	jugador2.g=0;
	jugador2.x1=6.0f;jugador2.y1=-1.0f;
	jugador2.x2=6.0f;jugador2.y2=1.0f;
	jugador2.parado=false;
	jugador2.tiempoParado=0;

}

///////////////////////////////////////////////////////////////////////////////
void CMundo::Disparando(Raqueta &player){
	
	
	if(player.x2<0.0f){
		float posx,posy,velx,vely;

		posx=player.x2+0.5f;
		posy=(player.y2+player.y1)/2;
		velx=10.0f;
		vely=0.0f;
		disparos.push_back(new Disparo(posx,posy,velx,vely,0.15f));
		
	}
	else if(player.x2>0.0f){
		float posx,posy,velx,vely;
		
		posx=player.x2-0.5f;
		posy=(player.y2+player.y1)/2;
		velx=-10.0f;
		vely=0.0f;
		disparos.push_back(new Disparo(posx,posy,velx,vely,0.15f));
			
	}
}

bool CMundo::ImpactoP1(Raqueta &player,Disparo *d){
	
	 if(d->velocidad.x<0.0f && (d->posicion.x<player.x2+MARGEN) && (d->posicion.x>player.x2-MARGEN) && (d->posicion.y>player.y1)&&(d->posicion.y<player.y2)){
		
		return true;
	}
	else return false;
}
bool CMundo::ImpactoP2(Raqueta &player,Disparo *d){
	
	
	 if(d->velocidad.x>0.0f && (d->posicion.x>player.x2-MARGEN) && (d->posicion.x<player.x2+MARGEN) && (d->posicion.y>player.y1) && (d->posicion.y<player.y2)){
		return true;
	}
	else return false;
}
void CMundo::Paralizar(Raqueta &player){
	
	if(player.parado==true){
		player.tiempoParado++;
			if(player.tiempoParado==75){
				player.tiempoParado=0;
				player.parado=false;
				
			}
	}
}

void CMundo::RecibeComandosJugador1()
{
	char cad[200];
     while (!acabar) {
            usleep(25000);
            if(conexiones.size()>0)
          
            conexiones[0].Receive(cad,sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            
    }
}
void CMundo::RecibeComandosJugador2()
{
	 char cad[200];
     while (!acabar) {
            usleep(25000);
            if(conexiones.size()>1)
          
            conexiones[1].Receive(cad,sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
            
      }
}
void CMundo::GestionaConexiones()
{
	while (!acabar)	{
	usleep(25000);
	conexiones.push_back(servidor.Accept());
	conexiones[conexiones.size()-1].Receive(nombre_c,sizeof(nombre_c));
	printf("El cliente %s se ha conectado al servidor\n",nombre_c);
	}
}
		

//////////////////////////////////////////////////////////////////////////////////////////

// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"



#include <string.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "stdio.h"
#include "stdlib.h"
#include <unistd.h>
#include <fcntl.h>
#define TAM_BUFF 1024
#define MARGEN 0.5f
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

char* org;

 CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	write(tuberia,"F",sizeof("F"));
	close(tuberia);
	if (munmap(org,sizeof(PunteroDatos)) == -1)
   	{
   	  close(fd_memoria);
  	  perror("Error un-mmapping the file");
   	}
      close(fd_memoria);
	
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
	
	
	/**/
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();
        for(int j=0;j<esferas.size();j++){
		esferas[j]->Dibuja();
        }
        for(int k=0;k<disparos.size();k++){
        disparos[k]->Dibuja();
        }
    fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();


	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)


{	



	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	for(int i=0;i<esferas.size();i++){
	esferas[i]->Mueve(0.025f);}
	for(int k=0;k<disparos.size();k++){
	disparos[k]->Mueve(0.025f);}
	int i;
	for(i=0;i<paredes.size();i++){
	for (int j=0;j<esferas.size();j++){
	
		paredes[i].Rebota(*esferas[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	  }
        }
        for (int j=0;j<esferas.size();j++){
	
	jugador1.Rebota(*esferas[j]);
	jugador2.Rebota(*esferas[j]);
        }
  //////////////////////////////////////////////////////////////////////////////////   
     for(int k=0;k<disparos.size();k++){
		 
			if(ImpactoP2(jugador2,disparos[k])){
				delete disparos[k];
				disparos.erase(disparos.begin()+k);
				jugador2.velocidad.y=0.0f;
				jugador2.parado=true;
				
			}
		
		else if(ImpactoP1(jugador1,disparos[k])){
				delete disparos[k];
				disparos.erase(disparos.begin()+k);
				jugador1.velocidad.y=0.0f;
				jugador1.parado=true;
				
			}
		
	
			if(disparos[k]->posicion.x>15.0f || disparos[k]->posicion.x<-15.0f){
				delete disparos[k];
				disparos.erase(disparos.begin()+k);
			}
		}
	
				
				
     this->Paralizar(jugador1);
     this->Paralizar(jugador2);   
 ////////////////////////////////////////////////////////////////////////////////////////       
    for (int i=0;i<esferas.size();i++){	
	if(fondo_izq.Rebota(*esferas[i]))
	{
		esferas[i]->centro.x=0;
		esferas[i]->centro.y=rand()/(float)RAND_MAX;
		esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
		esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
		++puntos2;
		char cad[200];
		sprintf(cad,"Jugador 2 marca 1 punto, lleva %d puntos", puntos2);
		write (tuberia, cad, strlen(cad)+1);
/////////////////////CONDICION DE SALIDA////////////////////////////////////////////////////		
		if (puntos2==3){
		printf("El ganador es el Jugador 2 \n");
		exit(0);
	}
		
	}

	if(fondo_dcho.Rebota(*esferas[i]))
	{
		esferas[i]->centro.x=0;
		esferas[i]->centro.y=rand()/(float)RAND_MAX;
		esferas[i]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esferas[i]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
		++puntos1;
		char cad[200];
		sprintf(cad,"Jugador 1 marca 1 punto, lleva %d puntos", puntos1);
		write (tuberia, cad, strlen(cad)+1);

/////////////////////CONDICION DE SALIDA////////////////////////////////////////////////////		
		if (puntos1==3){
		printf("El ganador es el Jugador 1 \n");
		exit(0);
	}
	}

	}
	Datos.esfera=*esferas[0];
	Datos.raqueta1=jugador2;
	PunteroDatos->esfera=*esferas[0];
	PunteroDatos->raqueta1=jugador2;	
	if(PunteroDatos->accion==1) OnKeyboardDown('o',1,1);
	if(PunteroDatos->accion==-1) OnKeyboardDown('l',1,1);
	
	

///////////////////////////////////////////////////////////////////////////////////	
	/*temporizador ++;
	if (temporizador==300 && esferas.size()<5){esferas.push_back(new Esfera);
	temporizador=0;}*/
///////////////////////////////////////////////////////////////////////////////////	


}
void CMundo::OnKeyboardDown(unsigned char key,int x,int y)
{
	
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	if(key == 's' && jugador1.parado==false){jugador1.velocidad.y=-4.0f;}
	if(key == 'w' && jugador1.parado==false){jugador1.velocidad.y=4.0f;}
	if(key == 'l' && jugador2.parado==false){jugador2.velocidad.y=-4.0f;}
	if(key == 'o' && jugador2.parado==false){jugador2.velocidad.y=4.0f;}
	if(key == 'f' && jugador1.parado==false){this->Disparando(jugador1);}
	if(key == 'j' && jugador2.parado==false){this->Disparando(jugador2);}
	
	
	
}

void CMundo::Init()
{
	
	temporizador=0;
	
	esferas.push_back(new Esfera);
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6.0f;jugador1.y1=-1.0f;
	jugador1.x2=-6.0f;jugador1.y2=1.0f;
	jugador1.parado=false;
	jugador1.tiempoParado=0;
	//a la dcha
	jugador2.g=0;
	jugador2.x1=6.0f;jugador2.y1=-1.0f;
	jugador2.x2=6.0f;jugador2.y2=1.0f;
	jugador2.parado=false;
	jugador2.tiempoParado=0;
	
	//ABRIMOS LA TUBERIA
	tuberia = open("/tmp/TuberiaLogger", O_WRONLY);
	
		Datos.esfera=*esferas[0];
		Datos.raqueta1=jugador2;		
		if((fd_memoria=open("/tmp/bot.txt", O_CREAT|O_RDWR,0666))<0)
		perror("Error al abrir/crear fichero");
		write(fd_memoria,&Datos, sizeof(Datos));	
		if (fstat(fd_memoria, &bstat)<0) {
		perror("Error en fstat del fichero"); 
		close(fd_memoria);
		}
		printf("tamanio fichero %ld ",bstat.st_size);

	//PROYECCIÓN DE MEMORIA


		//FALLO EN LA PROYECCIÓN
		if((org=(char*)mmap(NULL,sizeof(Datos), PROT_READ |PROT_WRITE ,MAP_SHARED,fd_memoria,0))==MAP_FAILED){ 
		perror("Error en la proyeccion del fichero\n"); 
		close(fd_memoria);
		}
		//PROYECCIÓN CON ÉXITO
		else printf("Memoria Proyectada\n");
		close(fd_memoria);
		PunteroDatos=(DatosMemCompartida*)org;
		PunteroDatos->accion=0;
		 
	
}
///////////////////////////////////////////////////////////////////////////////
void CMundo::Disparando(Raqueta &player){
	
	
	if(player.x2<0.0f){
		float posx,posy,velx,vely;

		posx=player.x2+0.5f;
		posy=(player.y2+player.y1)/2;
		velx=10.0f;
		vely=0.0f;
		disparos.push_back(new Disparo(posx,posy,velx,vely,0.15f));
		
	}
	else if(player.x2>0.0f){
		float posx,posy,velx,vely;
		
		posx=player.x2-0.5f;
		posy=(player.y2+player.y1)/2;
		velx=-10.0f;
		vely=0.0f;
		disparos.push_back(new Disparo(posx,posy,velx,vely,0.15f));
			
	}
}

bool CMundo::ImpactoP1(Raqueta &player,Disparo *d){
	
	 if(d->velocidad.x<0.0f && (d->posicion.x<player.x2+MARGEN) && (d->posicion.x>player.x2-MARGEN) && (d->posicion.y>player.y1)&&(d->posicion.y<player.y2)){
		
		return true;
	}
	else return false;
}
bool CMundo::ImpactoP2(Raqueta &player,Disparo *d){
	
	
	 if(d->velocidad.x>0.0f && (d->posicion.x>player.x2-MARGEN) && (d->posicion.x<player.x2+MARGEN) && (d->posicion.y>player.y1) && (d->posicion.y<player.y2)){
		return true;
	}
	else return false;
}
void CMundo::Paralizar(Raqueta &player){
	
	if(player.parado==true){
		player.tiempoParado++;
			if(player.tiempoParado==75){
				player.tiempoParado=0;
				player.parado=false;
				
			}
	}
}
		

//////////////////////////////////////////////////////////////////////////////////////////

// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_
#include <pthread.h>
#include <vector>
#include "Plano.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
//Librerias de Sockets
#include "Socket.h"
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "Disparo.h"
#include "Esfera.h"
//#include "Raqueta.h"
#include "../include/DatosMemCompartida.h"

class CMundo  
{
public:
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void Disparando(Raqueta& );
	bool ImpactoP1(Raqueta& ,Disparo *);
	bool ImpactoP2(Raqueta& ,Disparo *);
	void Paralizar(Raqueta& );	

	std::vector<Esfera *> esferas;
	std::vector<Plano> paredes;
	std::vector<Disparo *> disparos;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	int temporizador;
	//int temporizador2;
	
	
	int fd_memoria;
	struct stat bstat;	


	DatosMemCompartida Datos;
	DatosMemCompartida *PunteroDatos;

	
	
	int puntos1;
	int puntos2;
	int tuberiaLogger;
	int tuberia;
	int tuberia2;
	int acabar;
	pthread_t thid1; //Hilo para el jugador 1
	pthread_t thid2; //Hilo para el jugador 2
	pthread_t thid_conexiones;
	void RecibeComandosJugador1();
	void RecibeComandosJugador2();
	char nomClient[200];
	char cadena_n[200];
	char nombre_c[200];
	Socket servidor;
	std::vector<Socket> conexiones;
	void GestionaConexiones();
	int nombreCliente;


};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)

INSTRUCCIONES:


+Cada 12 segundos se irán añadiendo pelotas al escenario hasta un máximo de 5, que rebotarán contra las raquetas y las paredes.

+Las raquetas serán capaces de disparar proyectiles hacia el contrincante.

+En caso de colisión del proyectil lanzado por una raqueta a su contrincante, lo paralizará durante 3 segundos.

+Al estar paralizada una raqueta no podrá ni disparar ni moverse.